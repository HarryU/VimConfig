VimConfig
===============

Pull the repository to a any location. Create a symbolic link to the pulled .vimrc:

```bash
cd
git clone git@github.com:HarryU/VimConfig.git
ln -s ~/VimConfig/.vimrc ~/.vimrc
```
Now make sure the VIm is the full version with Python support:

```bash
sudo apt-get remove vim-tiny
sudo apt-get update
sudo apt-get install vim-nox
```

Open VIm, causing plugins to be installed.

Now compile YouCompleteMe:

```bash
sudo apt-get install build-essential cmake
cd ~/.vim/plugged/YouCompleteMe
python3 install.py --clang-completer --rust-completer
```

Install tmux and tmux plugin manager:
```bash
sudo apt-get install tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
ln -s ~/VimConfig/.tmux.conf ~/.tmux.conf
```

Install zsh, oh-my-zsh and the plugins:

```bash
sudo apt install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
mv ~/.zshrc ~/.zshrc.bak
ln -s ~/VimConfig/.zshrc ~/.zshrc
```

Links:

* [tmux cheatsheet](https://linuxacademy.com/blog/wp-content/uploads/2016/08/tmux-3.png)
* [vim cheatsheet](http://www.viemu.com/vi-vim-cheat-sheet.gif)
* [powerline docs](http://powerline.readthedocs.io/en/master/index.html)
